# List slicing
#
#   It means to slice and create a new list from a given list
#   We can specify the start and end index that we want to slice
#   Syntax:
#       list[start: end]            # get a new list from start to (end - 1) index
#       list[start: end: step]      # get a new list from start to (end - 1) index in step


#                      0            1           2           3           4
search_engines = ['google.com', 'ask.com', 'yahoo.com', 'bing.com', 'aol.com']
#                     -5            -4          -3          -2          -1

print(search_engines)

# Traditional approach to extract sub-list
sub_list = []
index = 0
for l_item in search_engines:
    if 1 <= index <= 3:
        sub_list.append(search_engines[index])
    index = index + 1
print(sub_list)

# Range example
tens = list(range(1, 101, 10))  # multiples of 10

print(tens[3:-1:])
print(f'search engine id: {id(search_engines)}')
sub_list = search_engines[1: 4]
print(sub_list)
print(f'search engine id: {id(sub_list)}')

# How to copy a list using slicing?
full_list = search_engines[0:]  # copies entire list

# How to copy a list using list slicing?
full_list = search_engines[:]  # copies entire list
print(full_list)
print(f'search engine id: {id(full_list)}')

# How to copy alternate elements in a list using list slicing?
skip_by_one = search_engines[0: 5: 2]
print(skip_by_one)

# How to traverse a list in reverse direction?
print(search_engines[3::-1])    # index 3 to last but one reverse traversal in steps of 1
print(search_engines[::-2])     # entire list reverse traversal in steps of 2

# How to reverse a list using slicing?
print(search_engines[::-1])

tens = list(range(0, 101, 10))
print(tens)

# reverse the list
print(tens[::-1])

# print in steps of two
print(tens[::2])

# using neg. indexing
print(tens[-6:])        # traverse from index -6 to the end of the list
print(tens[-6: -2])     # traverse from index -6 to -3 (-2 is exclusive)
