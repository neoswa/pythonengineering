from utils.common import show_id, listify_iterators

# 1. is vs "=="
#
# Both are conditional operators where:
#       "is" checks if two objects are equal, whereas
#       "==" two object's values are equal
#

# Equality of two lists example
l1 = [1, 2, 3, 4]
l2 = [1, 2, 3, 4]

print(l1 == l2)
print(l1 is l2)  # "is" will succeed, if two objects have same id

show_id(l1)
show_id(l2)

# Equality of two strings example
s1 = 'hello'
s2 = 'hello'

#        |         |
#        |         |
# s2 --> | 'hello' |
# s1 -^  |         |
#        |         |
show_id(s1)
show_id(s2)

print(s1 is s2)
print(s1 == s2)


# Lambda functions
#   - One line function definitions and it is anonymous
#   - Simple operations such as fetching an item in a list or tuple, or reverse an object etc.
#   - deprecated (tedious, poor readability)

# Normal function:
def function_name(arg_1, arg_2):
    return arg_1 * arg_2


multiply = lambda arg_1, arg_2: arg_1 * arg_2

print(multiply(3, 12))
print(multiply(3, 'a'))


# Create a function that copies and returns a list in reversed order
def get_reverse_list(in_list):
    """
        The function reverses and creates a new copy of a given list
    :param in_list: input list
    :return: copy of new list
    """
    rev_list = in_list[::-1]  # reverse and returns a copy
    # rev_list = list(reversed(in_list))  # reverse and returns a copy
    print(in_list)
    print(rev_list)
    return rev_list


reverser = lambda in_list: in_list[::-1]  # reverse and returns a copy

hobbies = [
    'carrom',
    'table tennis',
    'foose ball',
    'ps2'
]

# reverse_hobbies = get_reverse_list(hobbies)
reverse_hobbies = reverser(hobbies)

show_id(hobbies)
show_id(reverse_hobbies)

# Create a lambda function that accepts a tuple and always returns the second item
# (1, 2, 3) -> 2
# ('a', 'b', 'c') -> 'b'
# def fetch_second_item(tuple_):
#     return tuple_[1]


# Lambda approach:
fetch_second_item = lambda tuple_: tuple_[1]

# unit test
result = fetch_second_item(('hello world', 'python', 'program')) == 'python'
print(result)

result = fetch_second_item((1, (1,), [])) == 1
print(result)

# Dictionary sorting

hobbies = (
    'carrom',
    'table tennis',
    'foose ball',
    'ps2'
)
print(sorted(hobbies))  # returns a sorted list
city_temperatures = {
    'chennai': 35,
    'mumbai': 32,
    'delhi': 40,
    'kolkata': 29
}

print(city_temperatures)
listify_iterators(city_temperatures.items())
keys = city_temperatures.keys()
print(sorted(keys))

# The keyword arg reverse=True, will sort descending
sorted_by_value = sorted(city_temperatures.items(), key=fetch_second_item, reverse=True)  # pass the function w/o args
city_temperatures_sorted = dict(sorted_by_value)  # convert list of tuples to dict


print(sorted_by_value)
print(city_temperatures_sorted)

# Given the dictionary, find the city with second highest temperature
print(f'{sorted_by_value[1][0]} has second highest temperature!')
print(f'{sorted_by_value[-1][0]} has the lowest temperature!')
