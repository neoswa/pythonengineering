"""
Refer: https://www.geeksforgeeks.org/class-method-vs-static-method-python/
"""


class Calculator:
    """A simple pocket calculator"""

    def __init__(self, num_1, num_2):
        self.num_1 = num_1
        self.num_2 = num_2

    def add(self):
        """Adds num_1 and num_2 and returns it"""
        return self.num_1 + self.num_2

    def sub(self):
        """Subtracts num_2 from num_1 and returns it"""
        return self.num_1 - self.num_2


calc = Calculator(10, 20)

print(calc.num_1)
print(calc.num_2)

sum_added = calc.add()
print(sum_added)

diff = calc.sub()
print(diff)

# Deprecated
# calc = Calculator()
# calc.num_1 = 10
# calc.num_2 = 20


class HclEmployee:
    # Class variables (Class Constants) - Shared variable
    HR_HIKE_PERCENT = 0
    IT_HIKE_PERCENT = 0
    COUNT = 0

    def __init__(self, emp_id, name, dept, salary):
        """Creates object and initialized instance variables"""
        self.emp_id = int(emp_id)
        self.name = name
        self.dept = dept
        self.salary = int(salary)
        self.company = 'HCL'  # Restricts only to HCL emp
        HclEmployee.COUNT = HclEmployee.COUNT + 1

    def get_email(self):
        """
            Processes emp details to fetch the mail address

            @return     str     Employee mail id
        """
        email = f'{self.name}_{self.emp_id}@{self.company}.com'.lower()
        return email

    def perform_hike(self):
        if self.dept.lower() == 'it':
            self.salary = self.salary + (HclEmployee.IT_HIKE_PERCENT / 100 * self.salary)
        elif self.dept.lower() == 'hr':
            self.salary = self.salary + (HclEmployee.HR_HIKE_PERCENT / 100 * self.salary)

        self.salary = int(self.salary)
        print(f'Hike applied for {self.name}')

    @classmethod
    def modify_hr_hike(cls, new_hike):
        cls.HR_HIKE_PERCENT = new_hike

    @classmethod
    def modify_it_hike(cls, new_hike):
        cls.IT_HIKE_PERCENT = new_hike

    @classmethod
    def get_instance_from_csv(cls, record):
        """Creates instance out of some other data"""
        info_list = record.split(',')
        _, name, _, dept, _, emp_id, sal = info_list
        return cls(emp_id, name, dept, sal)

    @staticmethod
    def increment_employees(current_count, additional_count):
        # Standalone method that can be cut and placed outside
        return current_count + additional_count


record = '10202019,govind,20012018,it,computer,123,1000'  # csv
govind = HclEmployee.get_instance_from_csv(record)
print(govind.salary)
print(govind.get_email())

HclEmployee.modify_it_hike(20)
govind.perform_hike()
print(govind.salary)

# 2019 - company performed out of box and hence giving bonus to emps
# HclEmployee.IT_HIKE_PERCENT = 20
# HclEmployee.HR_HIKE_PERCENT = 15
HclEmployee.modify_hr_hike(15)
HclEmployee.modify_it_hike(20)

govind = HclEmployee(123, 'Govind', 'IT', 1000)
print(govind.get_email())

print(govind.salary)
govind.perform_hike()
print(govind.salary)

swadhi = HclEmployee(456, 'Swadhi', 'HR', 500)
print(swadhi.get_email())

print(swadhi.salary)
swadhi.perform_hike()
print(swadhi.salary)

HclEmployee(456, 'Swadhi', 'HR', 500)
HclEmployee(456, 'Swadhi', 'HR', 500)
HclEmployee(456, 'Swadhi', 'HR', 500)
HclEmployee(456, 'Swadhi', 'HR', 500)
HclEmployee(456, 'Swadhi', 'HR', 500)
HclEmployee(456, 'Swadhi', 'HR', 500)
HclEmployee(456, 'Swadhi', 'HR', 500)

print(f'Totally "{HclEmployee.COUNT}" employees are created!')

# info_list = record.split(',')
# print(info_list)
# print(len(info_list))

record = '10202019,govind,20012018,it,computer,123,1000'  # csv
_, name, _, dept, _, emp_id, sal = ['10202019', 'govind', '20012018', 'it', 'computer', '123', '1000']
govind = HclEmployee(emp_id, name, dept, sal)

_, name, _, dept, _, emp_id, sal = ['10202019', 'prabakar', '20012018', 'it', 'computer', '123', '1000']
prabakar = HclEmployee(emp_id, name, dept, sal)
print(HclEmployee.increment_employees(100, 150))
