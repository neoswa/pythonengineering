# Read a file
# Write to a file

# Reading a file
file, file_contents = None, None
file_lines = []

try:
    file = open('C:\\Users\\username\\Desktop\\sample.json', 'r')

    # Read file as a string
    file_contents = file.read()  # return the file as a string

    # Read file lines as a list
    file_lines = file.readlines()  # returns file lines as a list
    print(file_contents)
except FileNotFoundError as f:
    print(f'Error opening file. Check file exists. {f}')

try:
    new_file = open('C:\\Users\\username\\Desktop\\new_file.txt', 'w')
    # new_file.write(file_contents)

    # Write a list to a file
    print(file_lines[1:12])
    new_file.writelines(file_lines[1:12])  # takes a list and writes to the file directly

    # Write a string to file
    file_str = ''.join(file_lines[1:12])
    new_file.write(file_str)

    print('Copy successful')
except FileNotFoundError as f:
    print(f'Error opening file. Check file exists. {f}')
