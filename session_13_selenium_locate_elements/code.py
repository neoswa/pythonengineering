from selenium import webdriver
import time

"""
    What is Element Locator
        An attribute which is associated with a HTML element.
        This attribute can help us to locate the element from a web page

    Selenium provides the following methods to locate elements in a page:

        find_element_by_id
        find_element_by_name
        find_element_by_link_text
        find_element_by_partial_link_text
        find_element_by_tag_name
        find_element_by_xpath
        find_element_by_class_name
        find_element_by_css_selector

    To find multiple elements (these methods will return a list):
        find_elements_by_name
        find_elements_by_xpath
        find_elements_by_link_text
        find_elements_by_partial_link_text
        find_elements_by_tag_name
        find_elements_by_class_name
        find_elements_by_css_selector

    File Handling
"""

"""
Sample web elements:
    <input name='email' id='user'>Enter email</input>
    <input name='email' id='alt_user' >Enter alt. email</input>
    <input name='pass' id='pass' ></input>
"""


#
# # Sample script
def sample_script():
    url = 'https://www.facebook.com'

    # Launch browser
    driver = webdriver.Chrome()
    try:
        # Load URL
        driver.get(url)
        # Automation
    except Exception as e:
        print(f'Error while launching browser.\n{e}')
    finally:
        driver.quit()


def get_chrome_browser(url='https://www.google.com'):
    """
        Creates a browser instance, maximize and load url and return

    :param url: (optional)  Url to load
    :return:    chrome driver
    """
    driver_ = webdriver.Chrome()
    driver_.maximize_window()
    if url:
        driver_.get(url)
    return driver_


# Locate Element by ID
#     An element ID will be the unique identifier to locate an element
#     driver.find_element_by_id(<identifier>)
def test_gmail_login_field():
    url = 'https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin'
    driver = get_chrome_browser(url)

    # Element
    username_field = driver.find_element_by_id('identifierId')

    # Input field
    # element.clear() - clears the content
    # element.send_keys() - enters the text
    # element.click() - clicks the input field

    # username_field = driver.find_element_by_id('wrongId')   # incorrect id
    if username_field is not None:
        print('The login page is loaded')

    username_field.click()
    time.sleep(0.5)
    username_field.send_keys('cswadhik')  # incorrect
    time.sleep(0.5)
    username_field.clear()
    time.sleep(0.5)
    username_field.send_keys('cswadhikar@gmail.com')  # incorrect
    time.sleep(0.5)
    # username_field.submit()
    # time.sleep(1)
    next_button = driver.find_element_by_id('identifierNext')
    next_button.click()
    time.sleep(10)
    driver.quit()


# Locate Element by Name
#     We can identify an element by name when you know it's  name attribute
#     driver.find_element(s)_by_link_text(<name_attribute>)
def test_facebook_login_field():
    """
        Example function to find an element using "name" attribute
    """
    url = 'https://www.google.com/'
    search_text = "tamil songs"

    # Browser
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get(url)

    # Element
    search_field = driver.find_element_by_name('q')

    # Automation
    search_field.send_keys(search_text)
    search_field.submit()

    time.sleep(3)
    driver.quit()


# Locate Element by Link Text
#     We can identify an element by the text which represents the hyperlink
#     driver.find_element(s)_by_link_text(<link_text>)
#
def test_ask_culture():
    url = 'https://www.ask.com/'

    # Browser
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get(url)

    # Element
    culture_btn = driver.find_element_by_link_text('Culture')

    # Automation
    culture_btn.click()

    time.sleep(10)
    driver.quit()


# Locate Element by Partial Link Text
#     We can identify an element by a substring of text which represents the hyperlink
#     driver.find_element(s)_by_partial_link_text(<link_text>)
def test_forgotten_account():
    # Open browser and load facebook

    url = 'https://www.facebook.com'

    # Browser
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get(url)

    # Find the link and click
    driver.find_element_by_partial_link_text('account?').click()
    time.sleep(10)

    # verify Find Your Account in page
    verify_text = 'Find Your Account'

    if verify_text in driver.page_source:
        print('Forgotten account test has succeeded!')

    driver.quit()


print(f'Loaded file: {__file__}')

if __name__ == '__main__':
    test_gmail_login_field()
    test_facebook_login_field()
    test_ask_culture()
    test_forgotten_account()
