"""
    What is Selenium
    Features of Selenium
    The WebDriver Architecture
    Selenium Installation
    HTML Basics
"""

# What is Selenium WebDriver

#   - Collection of open source APIs (framework) which are used to automate web application testing
#   - Supports many browsers such as Firefox, Chrome, IE, and Safari, Opera and HtmlUnit
#   - It is supported by languages such as C#, Java, Perl, Python, PHP and Ruby
#     https://www.educative.io/api/edpresso/shot/4805499394457600/image/4626719703040000
#   - It is an enhancement to the selenium RC
#     https://www.educative.io/api/edpresso/shot/4805499394457600/image/5014315188879360


# Features of Selenium
#   - Reduces man hours in manual efforts in testing
#   - Fast compared to manual
#   - Can test 24*7 (Regression testing)
#   - Enables testing in volumes


# The WebDriver Architecture
# https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2019/05/Picture5-768x320.png
# Firefox - geckodriver
# Chrome  - chromedriver.exe    https://chromedriver.chromium.org/downloads
# echo %PATH%


# Selenium Installation
# python -m pip install selenium

"""
    Understand the basics of HTML, Tags, Attribute and Elements
        Examples of web elements in the page:
            Input field - <Input> tag
            Button - <button> tag
            Hyperlinks - <a> tag
            Drop down - <select> and <option>   
            Image - <img> tag
        
        Attributes of an element:
            <tag attr_1=val_1 attr_2=val_2  ></tag>
            
            <input name='email' id='user' ></input>
            <input name='pass' ></input>

"""

# Automation:
# Load page - driver.get(url)
# Interact with elements
