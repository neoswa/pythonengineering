"""
    Capture screenshots             - driver.save_screenshot(filename)
    Toggle tabs back and forth      - driver.execute_script( window.open('https://www.google.com'); )
    Scrolling down(+ve) and up(-ve) - driver.execute_script( window.scrollTo(0, 100) )
    Mouse Hover                     - ActionChains(driver).move_to_element(WebElement).perform()
    Right Click                     - ActionChains(driver).context_click(WebElement).perform()
    Double Click                    - ActionChains(driver).double_click(WebElement).perform()

    driver.execute_script(script)   - Executes the given javascript
    driver.save_screenshot          - Screen shot capturing
    driver.switch_to.window         - attribute that enables switching tab
    window_handles                  - it is a list of tabs in sequential order

    Action chains syntax:

    action = ActionChains(driver)
    action.operation_1(web_element)
    action.operation_2(web_element)
    action.move_to_element(web_element)
    action.perform()
"""

from session_15_handle_web_objects_1.code import get_chrome_browser, google_search
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.action_chains import ActionChains

import time


def download_file(driver, link_text='Python-3.7.4.tar.xz.asc'):
    driver.find_element_by_link_text(link_text).click()


# Toggle windows back and forth
def open_new_tab(driver, main_url='https://google.com', new_tab_url=''):
    """Opens an empty tab and toggles to first tab"""

    driver.get(main_url)

    new_window_script = f"window.open('{new_tab_url}');"
    driver.execute_script(new_window_script)

    # Toggle
    print('window handles:', driver.window_handles)

    main_window = driver.window_handles[0]
    new_tab = driver.window_handles[-1]
    driver.switch_to.window(new_tab)

    if "Instagram" in driver.page_source:
        print('Instagram has been opened')

    time.sleep(2)

    # Go back to main window
    driver.switch_to.window(main_window)
    time.sleep(2)

    # Close the current tab
    driver.close()  # ctrl + 'w'
    time.sleep(2)


# Scrolling web pages
def scroll_down(driver, scroll_by=300):
    script = f"window.scrollTo(0, {scroll_by})"
    driver.execute_script(script)


def scroll_to_bottom(driver):
    script = "window.scrollTo(0, document.body.scrollHeight);"
    driver.execute_script(script)


def hover_action(driver, element):
    # Move to web element
    action = ActionChains(driver).move_to_element(element)  # moves the cursor to the given element

    # Perform the operation using .perform() method
    action.perform()

    time.sleep(5)


def upload_image(driver, filepath):
    # Identify Choose button
    time.sleep(2)

    choose_btn = driver.find_element_by_name('upfile')
    choose_btn.send_keys(filepath)
    time.sleep(2)

    driver.find_element_by_xpath('//input[@value="Press"]').click()
    driver.save_screenshot('bytes.png')
    time.sleep(2)


def right_click(driver, web_element):
    # Move to web element
    action = ActionChains(driver).context_click(web_element)

    # Perform the operation using .perform() method
    action.perform()

    time.sleep(3)


def get_web_element_from_xpath(driver, xpath):
    return driver.find_element_by_xpath(xpath)


if __name__ == '__main__':
    print(f'Execution started from file: {__file__}')

    driver = None
    try:
        # Right Click element
        driver = get_chrome_browser('https://www.amazon.in')
        sign_in_xpath = '//span[text()="Hello. Sign in"]'
        right_click(driver, get_web_element_from_xpath(xpath=sign_in_xpath, driver=driver))

        # Capture screen
        driver = get_chrome_browser()  # returns a chrome browser with google.com opened
        google_search(driver, 'Indo China Summit')  # pass the browser

        # New tab
        driver = get_chrome_browser()  # returns a chrome browser with google.com opened
        open_new_tab(driver)
        print('New tab has been opened')
        open_new_tab(driver, new_tab_url='https://www.instagram.com')

        # Hover over element
        hover_action(driver, get_web_element_from_xpath(driver, '//*[@title="Google apps"]'))

        # Upload image
        driver = get_chrome_browser('https://www.amazon.in/')
        hover_action(driver, get_web_element_from_xpath(driver, "//*[contains(text(), 'Try Prime')]"))

        driver = get_chrome_browser('https://cgi-lib.berkeley.edu/ex/fup.html')
        upload_image(driver, filepath='C:\\Users\\schandramohan\\Pictures\\wallpaper\\180681.jpg')

        # Download file
        driver = get_chrome_browser('https://www.python.org/ftp/python/3.9.0/')
        scroll_to_bottom(driver)  # move to last
        time.sleep(2)
        download_file(driver, link_text='python-3.9.0a2-webinstall.exe.asc')
        time.sleep(2)

    except NoSuchElementException as e:
        print(f'Could not find element. Caused by: {e}')
    finally:
        print('Cleanup started: Closing browser!')
        driver.quit()
