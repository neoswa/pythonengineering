how to remove repetitive elements in a list?

    my_list = ["a", "b", "a", "c", "c"]
    new_dict = dict.fromkeys(my_list)
    # dict.fromkeys() will remove duplicates and create an None valued list
    # {'a': None, 'b': None, 'c': None}
    my_list = list(new_dict)
    print(my_list)


how to sort lists based on key?
chiru dhuva
swadhi chand
govind kaluva


How to find a last occurrence of an element in a list?
    >>> l = ['a', 'b', 'a', 1, 2, 3]
    >>>
    >>> i = -1
    >>> for e in l[::-1]:
    ...     if 'a' == e:
    ...         break
    ...     i = i - 1
    ...