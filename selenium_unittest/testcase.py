from selenium import webdriver

import unittest


class TestAmazon(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.amazon_url = 'https://www.amazon.in/'
        self.driver.get(self.amazon_url)

    def test_browser_launched(self):
        """Tests if a browser is launched"""
        self.assertIsNotNone(self.driver)
        self.assertIsInstance(self.driver, webdriver.Chrome)

    def test_amazon_loaded(self):
        self.assertEquals(self.amazon_url, self.driver.current_url)

    def test_best_sellers(self):
        link_text = "Best Sellers"
        self.driver.find_element_by_link_text(link_text).click()
        link_text_confirm = "See more Bestsellers in Books"
        self.assertTrue(link_text_confirm in self.driver.page_source)

    def tearDown(self) -> None:
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
