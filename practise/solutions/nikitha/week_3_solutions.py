# 1. Dictionary looping and builtin functions:
#
# Given the below dictionary:
# teams = {'Colorado': 'Rockies', 'Boston': 'Red Sox', 'Minnesota': 'Twins', 'Milwaukee': 'Brewers', 'Seattle': 'Mariners'}
#
# 1. Write a simple for loop that:
#      a. prints only the keys
#      b. prints only the values
#      c. prints the keys and values in format:
#             key1~value1
#             key2~value2

teams = {'Colorado': 'Rockies', 'Boston': 'Red Sox', 'Minnesota': 'Twins', 'Milwaukee': 'Brewers',
         'Seattle': 'Mariners'}
for team in teams:
    print(team)
for team in teams.values():
    print(team)
for k, v in teams.items():
    print(f'the key value pair, {k} ~ {v}')

# Given the below dictionary:
family = {'fname': 'Joe', 'lname': 'Fonebone', 'age': 51, 'spouse': 'Edna', 'children': ['Ralph', 'Betty', 'Joey'],
          'pets': {'dog': 'Fido', 'cat': 'Sox'}}
#
# 2. Write a simple for loop that prints.
#    a. each child's name in a separate line
#    b. each pet's name in format:
#         "dog's name is ..."
#         "cat's name is ..."
#    c. Now add another pet of type rabbit with name bunny and pretty print the dictionary
#    d. Now update the dict in qn. 1 to this dictionary with key as 'teams'
#

family = {'fname': 'Joe', 'lname': 'Fonebone', 'age': 51, 'spouse': 'Edna', 'children': ['Ralph', 'Betty', 'Joey'],
          'pets': {'dog': 'Fido', 'cat': 'Sox'}}
childs_name = family['children']
for n in childs_name:
    print(n)

pets_name = family['pets']
print(pets_name)
for p, c in pets_name.items():
    print(f' {p} name is ... {c}')

family['bunny'] = 'rabbit'
from pprint import pprint       # @swadhi: all the import statements should be at the top of the module

pprint(family)
family.update(teams)
print(family)

# Given the below dictionary
# d = {0: 'a', 1: 'a', 2: 'a', 3: 'a'}
#
# 3. Using for loop print the sum of all the keys
d = {0: 'a', 1: 'a', 2: 'a', 3: 'a'}
sum = 0
for k in d.keys():
    sum += k
print(sum)

# 4. Use a for loop to append all the values into a single string
single_string = ''
for v in d.values():
    single_string += v
print(single_string)

# 5. What is the purpose of dict.clear() method? Give example.
# dict.clear() method is to clear all the contents in the dictionary

# 6. Empty all the above dictionaries and print of them. THe contents should be empty.
d.clear()
family.clear()
teams.clear()
print(d)
print(family)
print(teams)

# 7. Now, delete all the above dictionaries totally from the memory.
del teams
del family
del d
print(f'{teams}, {family}, {d}')
# Got nameError

# Searching items:
#
#
random_list = [21.42, 'foobar', 3, 4, 'bark', False, 3.14159]
# Given the above list,
#
# 8.  Search and print True or False, if 5 exists in the list'
if 5 in random_list:
    print(True)
else:
    print(False)

# 9.  Search and print True or False, if any number between 20 to 25 exists in the list
for number in random_list:
    if type(number) == int or type(number) == float:
        print(number)
        if number >= 20 and number <= 25:   # @swadhi: 20 <= number <= 25
            print(number)

# 10. Search and print if any string which has a substring of foo
for sub_string in random_list:
    if type(sub_string) == str:
        if 'foo' in sub_string:
            print(sub_string)

# 11. Search and print any boolean value
for value in random_list:
    if type(value) == bool:
        print(value)

# 12. Search and print any number that is a perfect square. [google to find how a number is a perfect square]
import math     # @swadhi: import at the top

random_list = [21.42, 'foobar', 3, 4, 'bark', False, 3.14159]
for value in random_list:
    if type(value) == int or type(value) == float:
        n = math.sqrt(value)
        # print(n)
        m = n - math.floor(n)
        # print(round(m))
        if m == 0:
            print(f'{value} is a prefect square')
        else:
            print(f'{value} is not prefect square')


# Functions
# Write a function with name "get_employee_badge"
#       1. Arguments: 3 mandatory arguments - emp_id, first_name, last_name,
#       2. Inside the function check if the emp_id parameter is an integer:
#               if not, then print "Invalid employee id: <emp_id>"
#               else, print a string that looks like: '<emp_id> : <first_name>:<last_name>'
#
# Sample solution:
# def get_employee_badge(emp_id, first_name, last_name):
#     if type(emp_id) != int:
#         print('Invalid employee id:', emp_id)
#     else:
#         print(emp_id, first_name, last_name, sep=':')
#
#
# get_employee_badge('wrong', 'swadhikar', 'chandramohan')  Invalid employee id: wrong
# get_employee_badge(423367, 'swadhikar', 'chandramohan')  423367:swadhikar:chandramohan
#
#
# 13. Write a function with name "get_mail_id"
#       1. Arguments: 3 mandatory arguments - first_name, last_name, company_name
#       2. Inside the function check if the company_name parameter is a string whose size is lesser than or equals 5:
#               if the size is greater than 5, then print "Company acronym can have a max size of 5 characters"
#               else, return a mail id of format: <firstname>.<lastname>@<company>.com


def get_mail_id(first_name, last_name, company_name):
    if type(company_name) == str and len(company_name) <= 5:
        print(f'the mail id of the employee is : {first_name}.{last_name}@{company_name}.com')
    else:
        print("Company acronym can have a max size of 5 characters")    # @swadhi: "Company acronym must have ..."


get_mail_id('Nikitha', 'Yadav', 'HCL')
get_mail_id('Nishanth', 'Yadav', 'Amazon')


# 14 . Write a function with name "get_emp_grade"
#       1. Arguments:
#                       2 mandatory arguments - "emp_id", "first_name"
#                       1 default argument    - "experience" with initial value of 0
#       2. Inside the function,
#               if the experience is lesser than 1, then print "<emp_id> is a fresher!"
#               if the experience is greater than 1 and lesser than 3, then return "<emp_id> is a junior developer"
#               if the experience is greater than 3, then return "<emp_id> is a senior developer"
#       3. Make three kinds of calls to this function:
#               call 1 - do not pass the parameter "experience" and print the result
#               call 2 - pass the parameter "experience" lesser than 3 and print the result
#               call 1 - the parameter "experience" greater than 3 and print the result

def get_emp_grade(emp_id, first_name, experience=0):
    if experience < 1:
        print(f'{emp_id} is a fresher')             # @swadhi: printf(f'{first_name} is a ...')
    elif experience > 1 and experience < 3:         # @swadhi: 1 < experience < 3
        print(f'{emp_id} is a junior developer')
    elif experience > 3:
        print(f'{emp_id} is a senior developer')


get_emp_grade(220546, 'nishanth')
get_emp_grade(220547, 'nikitha', 2.6)
get_emp_grade(220548, 'Siri', 4)


# 15. Write a function with name "get_circle_volume"
#       1. Arguments:
#                       1 default argument - "radius" with default value 0
#       2. Inside the function,
#               if the radius is lesser than 0 or equal to 0, then return 0,
#               else return the circle volume (volume formula: (4/3) * (22/7) * r ** 3)
#       3. Make three kinds of calls to this function:
#               call 1 - do not pass the parameter "radius" and print the result
#               call 2 - pass the parameter "radius" lesser than 0 and print the result
#               call 1 - the parameter "radius" greater than 0 and print the result

def get_circle_volume(radius=0):
    if radius <= 0:
        return 0
    else:
        return ((4 / 3) * (22 / 7) * radius ** 3)   # @swadhi: Outermost braces not required


r1 = get_circle_volume()
print(f'the circle volume is : {r1}')
r2 = get_circle_volume(-1.5)
print(f'the circle volume is : {r2}')
r3 = get_circle_volume(2)
print(f'the circle volume is : {r3}')


# 16. Write a function with any name that accepts one default argument at first
#     followed by a mandatory argument and note the result. (hint: f(d=10, m) )

# def f(d=10, name):
#     print(f'{d}, {name}')
#
# f(25,'nikitha')
# #
# # SyntaxError: non-default argument follows default argument


# 17. Write a function with any name that accepts one default argument at first
#     followed by a mandatory argument and note the result. (hint: f(d=10, m) )
# same as 16

# Sample question for *args:
# Write a function that accepts any number of integer arguments and returns the product of all its numbers
def get_product(*numbers):
    product = 1
    for number in numbers:
        product = product * number
    return product


# 18. Write a function that accepts any number of integer arguments and returns the difference of all its numbers

def get_subtraction(*numbers):
    difference_value = 0
    for number in numbers:
        difference_value = difference_value - number
    return difference_value


d = get_subtraction(1, 2)
print(f'the difference value is {d}')


# 19. Write a function that:
#       a. Accepts one mandatory argument - student_id
#       b. Any number of marks as *marks
#    Inside the function check:
#       c. If *marks has any content, then calculate the sum of all marks and return total marks of a student
#       d. Else, return 0
#
def function(student_id, *marks):
    sum = 0
    if len(marks) > 0:
        for mark in marks:
            sum = sum + mark
            print(f'the student id with {student_id} secured : {sum}')
    else:
        return 0


function(95, 20, 30, 40, 50)


# 20. Write a function that has any number of keyword arguments (kwargs) that has keys
#    as student ids in a classroom and his/her marks as value. For example, 'Student'=1800, 'Student2'=900
#    The function should return the total marks scored by all students in the class

def total_marks(**kwargs):
    sum = 0                     # @swadhi: sum is a builtin function. we need to use sum_ or _sum to differentiate
    for marks in kwargs.values():
        sum = sum + marks
    return sum


t = total_marks(Student=1800, Student2=900)
print(f'the total marks secured by students: {t}')


# 21. Write a function that has:
#       Arguments:
#           a. party name (mandatory)
#           b. variable number of keyword arguments with district names as keys and votes secured as values
#                   For example: chennai=18000, trichy=7800 and so on.
#       The function should calculate the total votes and print "<partyname> has got a total of <totalvotes> votes'

def total_votes(party_name, **district_votes):
    total_votes = 0
    for vote in district_votes.values():
        total_votes = total_votes + vote
    print(f'the total votes for {party_name} are {total_votes} ')


total_votes('CPI', chennai=18000, trichy=7800)


# 22. Write a function that has:
#       Arguments:
#           a. party name (mandatory)
#           b. variable number of keyword arguments with district names as keys and votes secured as values
#               For example: chennai=18000, trichy=7800 and so on.
#
#       The function should:
#           1. Check if the **kwargs has any content in it. If not, then simply return the function
#           2. else, calculate the total votes and print "<partyname> has got a total of <totalvotes> votes'

def total_votes(party_name, **district_votes):
    if type(district_votes) is dict and len(district_votes) > 0:
        total_votes = 0
        for vote in district_votes.values():
            total_votes = total_votes + vote
        print(f'the total votes for {party_name} are {total_votes}')
    else:
        return None


total_votes('CPI', chennai=18000, trichy=7800)
total_votes('CPI', chennai=0, trichy=0)
