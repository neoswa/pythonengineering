"""
    Summary:
      Functions with
          1. mandatory arguments
          2. default arguments
"""

"""
# Functions uses:
# 1. Organize code - each function will do individual task
    # open_google(): open google.com
    # login(username, password)
# 2. Code reuse
    # DRY - dont repeat yourselves
    
 - are first class objects
    
Function definition:
    - a function with more than 50 lines is incorrect    
"""

"""
Checks, validations etc.
          |           |
          |  Function |  
          |           |


Updates to DB:
          |           |
Input --> |  Function |  
          |           |


          |           |
Input --> |  Function |  ---> Output
          |           |
"""
"""
def func_name(param_1, param_2, ... ):
    # Start of the block
    # statement 1
    # statement 2
    # ...
    # ...
    pass 
# End of the block
"""

# How you will modify a variable inside a function?
# global keyword
student_list = []  # shared resource


# Function definition
def show_students():
    global student_list
    student_list = ['govind', 'brindha', 'chiru', 'niki']  # scope died within the function
    for student in student_list:
        print(student, end='****')


def add(a, b):  # mandatory args
    sum = a + b  # processing
    return sum


added = add(10, 20)
print(added)


#   concatenate_lists(l_1,    l_2)
def concatenate_lists(list_1, list_2):
    """
    The function that accepts two lists and concatenates them

    :param list_1:  input list 1
    :param list_2:  input list 2
    :return:        concatenated list
    """
    # Check if the arguments are lists
    if type(list_1) is list and type(list_2) is list:
        list_1.extend(list_2)
        return list_1
    else:
        print(f'Operation not possible with types:', type(list_1), type(list_2))
        return None


l_1 = ['gov', 'chi']
l_2 = ['bri', 'nik']

r_list = concatenate_lists(l_1, l_2)
print(r_list)
r_list = concatenate_lists(l_2, l_1)
print(r_list)
r_list = concatenate_lists(l_2, 2)
print(r_list)

concatenate_lists()


# Traceback (most recent call last):
#   File "<stdin>", line 1, in <module>
# TypeError: concatenate_lists() missing 2 required positional arguments: 'list_1' and 'list_2'

# Caller
# show_students()
# print('\n', student_list)

def get_circle_area(radius=10):
    print(f'radius = {radius}')
    return 22 / 7 * radius ** 2


area = get_circle_area()
print(area)

area = get_circle_area(7)
print(area)


def concatenate_lists(list_1=[], list_2=[]):
    """
    The function that accepts two lists and concatenates them

    :param list_1:  input list 1
    :param list_2:  input list 2
    :return:        concatenated list
    """
    print(f'list1: {list_1}\nlist2: {list_2}')

    # Check if the arguments are lists
    if type(list_1) is list and type(list_2) is list:
        list_1.extend(list_2)
        return list_1
    else:
        print(f'Operation not possible with types:', type(list_1), type(list_2))
        return None


# appended = concatenate_lists()
# appended = concatenate_lists(['fruit', 'car'], ['games', 'chocolates'])
appended = concatenate_lists(list_2=['fruit', 'car'], list_1=['games', 'chocolates'])

print(f'Appended list: {appended}')


def greet(name, msg="Good morning!"):
    """
    This function greets to
    the person with the
    provided message.

    If message is not provided,
    it defaults to "Good
    morning!"
    """

    print("Hello", name + ', ' + msg)


# greet("Kate")
greet("Bruce", "How do you do?")
