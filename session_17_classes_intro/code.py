"""
    1. Creating a class
    2. Instantiating objects
    3. Class Variables
    4. Instance variables
"""
import time


# Object oriented programming
#    - Helps to structure the program
#    - Properties and behaviors are bundled into individual objects

# Example:
#  Person - age, address properties and behaviors like walking, talking, etc.


def show_area():
    """ Fetches some area """
    # radius calculate
    # area finding
    pass


"""
# Area
# circum
# volume

class Circle:
    def get_circum(self):
        ...
    
    def get_volume(self):
        ...
"""


# couch
# no of seats
# guarantee
# material


# Class:
#   - Provides a user-defined data structure
#   - It is a mean to template (or blueprint) "properties" (what an object has - variables)
#     and "behaviours" (what an object does - methods) of an object

class ClassName:
    pass


class_ = ClassName()


# Object (or instance):
#   - It is a object form of class template with actual values
#   - Occupies physical space in the memory stack

class CustomDS:
    emp_list = list()
    designation = dict()


# https://phpenthusiast.com/theme/assets/images/articles/classes_and_objects.jpg

# Examples:
"""
    Aircraft
        Attributes:
         - maximum speed (changed)
         - available seats
        
        Behaviours:
         - take off
         - fly
         - land
"""

"""
    Laptop:
        Attributes:
         - number of cores
         - hard disk space
         - RAM
        
        Behaviours:
         - run multiple processes
         - play media
"""

"""
    Programmer
        Attributes:
         - emp_id
         - fullname
         - role
        
        Behaviours:
         - program code
         - debug issues
         - code reviews
         - take scrum
"""


# # OOP
# # Template -> Class
# class Programmer:
#     pass
#
#
# print(Programmer)
#
# # Object creation -> Instantiation
# prg = Programmer()
# prabakar = Programmer()
#
# print(prg)           # <__main__.Programmer object at 0x007709D0>
# print(prabakar)      # <__main__.Programmer object at 0x016E0910>
#

class Programmer:  # 1,    'Govind', 'STL'
    def __init__(self, emp_id, fullname, role):
        self.emp_id = emp_id
        self.fullname = fullname
        self.role = role

    def do_program(self):
        print(f'{self.fullname} can do python programming')


prg = Programmer(1, 'Govind', 'STL')
prg2 = Programmer(2, 'Nikitha', 'E')
prg3 = Programmer(3, 'Prabakar', 'TL')

Programmer.do_program(prg)  # prg.do_program()
Programmer.do_program(prg2)
Programmer.do_program(prg3)

print(prg.fullname)
print(prg2.fullname)
print(prg3.fullname)


class TrafficSignal:
    # - Attribute
    #   signal_color
    def __init__(self, color_1, color_2, color_3):
        self.start_color = color_1
        self.warning_color = color_2
        self.stop_color = color_3

    # - Behaviours
    #   signal start
    def start(self):
        print(f'{self.stop_color} -> {self.start_color}: You may start now!')

    #   signal warning
    def warn(self):
        print(f'{self.start_color} -> {self.warning_color}: Please reduce the speed')

    #   signal stop
    def stop(self):
        print(f'{self.warning_color} -> {self.stop_color}: Please stop your vehicle')


signal = TrafficSignal('green', 'orange', 'red')

print(dir(signal))
print(f'Stop color: {signal.stop_color}')

for _ in range(3):
    signal.warn()
    time.sleep(2)

    signal.stop()
    time.sleep(5)

    signal.start()
    time.sleep(2)
