import time

from session_13_selenium_locate_elements.code import get_chrome_browser
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.select import Select

"""
    WebElements attributes
        is_displayed()
        is_enabled()
        tag_name
        get_attribute()
        text

    Input field attributes:
        clear
        click

    Link attributes:
        click

    Button attributes:
        click 
        submit - submits the entire form to the server. 

    Radio Button //input[@type="radio"]:
        click
        is_selected

    Dropdown    
        WebElement = driver.find_element_by_xxx()
        Select(WebElement)
        select_by_index()
        select_by_visible_text()
        select_by_value()
        
    Exceptions
        NoSuchElementException
        StaleElementReferenceException
"""

# 1. Define URL to work with
google_url = 'https://www.google.com'
fb_url = 'https://www.facebook.com'
executable_path = '/home/swadhi/Documents/driver/chromedriver'


# Function to load Chrome Browser


def google_search(driver, search_text="Today's weather in Chennai"):
    # Get search box element
    search_box = driver.find_element_by_name('q')
    search_box.submit()

    # Log the tag of search box
    print('Tag of the search box:', search_box.tag_name)

    if search_box.is_displayed() and search_box.is_enabled():
        search_box.send_keys(search_text)
        time.sleep(1)
        search_box.submit()
    else:
        print('Unable to find the search box')


def google_clear_search(driver, search_text, wrong_search_text='some irrelevant text'):
    """
        Input field attributes:
            clear
            click
    """
    # Page load
    driver.get(google_url)

    # Get element
    search_box = driver.find_element_by_name('q')  # @name = q

    # Enter text
    search_box.send_keys(wrong_search_text)
    time.sleep(1)

    # Clear the content
    search_box.clear()
    print('Search field is cleared. Entering search text ...')
    time.sleep(1)

    search_box.send_keys(search_text)
    search_box.submit()
    time.sleep(5)


def facebook_gender(driver):
    """ Radio button and dropdown example """
    # Load page
    female_xpath = "//input[@type='radio' and @value='1']"
    custom_xpath = "//input[@type='radio' and @value='-1']"
    pronoun_xpath = '//select[@name="preferred_pronoun"]'

    # Find custom radio element
    custom_element = driver.find_element_by_xpath(custom_xpath)
    custom_element.click()

    # driver.save_screenshot('custom_options.png')

    # # Use select to work with the dropdown
    pronoun_element = driver.find_element_by_xpath(pronoun_xpath)
    pronoun_drop_down = Select(pronoun_element)  # pass the dropdown web element as argument

    # Using the dropdown index
    for number in range(1, 5):
        pronoun_drop_down.select_by_index(number)
        time.sleep(2)

    visible_text = [
        'She: "Wish her a happy birthday!"',
        'He: "Wish him a happy birthday!"',
        'They: "Wish them a happy birthday!"',
        'Inexistent text'
    ]

    for drop_down_value in visible_text:
        pronoun_drop_down.select_by_visible_text(drop_down_value)
        time.sleep(2)

    value_list = [1, 2, 6, 100]  # 'Select your pronoun'

    for drop_down_value in value_list:
        pronoun_drop_down.select_by_value(str(drop_down_value))
        time.sleep(1)

    # Note: select_by_value needs "value" attribute to be present in the WebElement
    time.sleep(1)


print(f'Loaded file: {__file__}')

if __name__ == '__main__':

    driver = None
    try:
        driver = get_chrome_browser(fb_url)

        username = driver.find_element_by_name('email')
        print(username)

        password = driver.find_element_by_name('pass')

        # for attr in dir(username):
        #     if '__' in attr:
        #         continue
        #     print(attr)

        print(f'The username field is_displayed: {username.is_displayed()}')
        print(f'The username field is_enabled: {username.is_enabled()}')
        print(f'The username field is_selected: {username.is_selected()}')

        username.click()  # operations
        # username.screenshot('first.png')
        username.send_keys('cswadhikar@gmail.com')  # operations
        # username.screenshot('second.png')
        time.sleep(1)

        username.clear()  # operations
        username.send_keys('9176155349')

        print('Username field is populated !')

        """
        Sending passwords to the browser:
            import os
            pwd_txt = os.environ['SECRET_PWD']
            print('Entering password ...')
            password.send_keys(pwd_txt)
        """

        google_search(driver)  # pass the browser
        facebook_gender(driver)

        signup_btn = driver.find_element_by_name('websubmit')
        print(f'Text discovered text: {signup_btn.text}')
        print(f'Text discovered tag_name: {signup_btn.tag_name}')

        help_link = driver.find_element_by_link_text('Help')

        print(f'The help link is_displayed: {help_link.is_displayed()}')
        print(f'The help link is_enable: {help_link.is_enabled()}')

        time.sleep(3)
        help_link.click()
        time.sleep(3)

        driver = get_chrome_browser(google_url)
        google_search(driver)
        google_clear_search(driver, search_text='india vs west indies score')

        driver = get_chrome_browser(fb_url)
        facebook_gender(driver)

    except NoSuchElementException as e:
        print('Invalid option:', e)
    finally:
        print('Cleanup started: Closing browser!')
        driver.quit()
