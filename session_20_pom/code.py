from selenium.webdriver.support.select import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium import webdriver
import time


class PageElement:
    def __init__(self, locate_by, locator, driver):
        self.by = locate_by
        self.locator = locator
        self.driver = driver

    @property
    def element(self):
        return self.driver.find_element(self.by, self.locator)

    def click(self):
        self.element.click()

    def enter_tab(self, post_wait=2):
        self.element.send_keys(Keys.TAB)
        time.sleep(post_wait)


class Input(PageElement):
    def enter_text(self, text):
        self.element.click()
        self.element.send_keys(text)


class RadioButton(PageElement):
    # def click(self):
    #     self.element.click()
    pass


class Dropdown(PageElement):
    def select_value(self, option):
        return Select(self.element).select_by_value(str(option))

    # def select_index(self, option):
    #     return Select(self.element).select_by_value(option)
    #
    # def select_visible_text(self, option):
    #     return Select(self.element).select_by_value(option)
    #


class CheckBox(PageElement):
    def select_element(self):
        if not self.element.is_selected():
            self.element.click()

    def un_select_element(self):
        if self.element.is_selected():
            self.element.click()


class Button(PageElement):
    pass


class Link(PageElement):
    pass


class Page:
    """
        Base page for all the web pages. It houses some basic page operations such as close browser,
        scroll down, scroll up etc.

        The classes inheriting this page should contain page attributes and page actions.

        Page Attributes are the web elements present in the web page. Input, Button, Link and so on.
        It is usually decorated with @property to make it as a instance variable.

        Page Action performs certain functionality using the page elements present in that particular page.
    """

    def __init__(self, url):
        self.url = url
        self.driver = self.create_browser()

    def create_browser(self, maximize=True):
        driver = webdriver.Chrome()
        driver.get(self.url)
        if maximize:
            driver.maximize_window()
        time.sleep(2)
        return driver

    def close_browser(self):
        self.driver.quit()

    def scroll_down(self, scroll_by=300):
        script = f"window.scrollTo(0, {scroll_by})"
        self.driver.execute_script(script)

    def scroll_up(self, scroll_by=300):
        scroll_by = scroll_by * -1
        script = f"window.scrollTo(0, {scroll_by})"
        self.driver.execute_script(script)


class Symantec(Page):
    SYMANTEC_URL = 'https://www.symantec.com/'

    def __init__(self):
        super().__init__(Symantec.SYMANTEC_URL)

    @property
    def sign_in_link(self):
        """Link Element"""
        sign_in_xpath = '//div[@class="auth-selector not-logged-in"]//div[1]//a[1]'
        return Link(By.XPATH, sign_in_xpath, self.driver)

    @property
    def create_account_button(self):
        """Tab element"""
        create_account_id = 'registerToggleButton'
        return Button(By.ID, create_account_id, self.driver)

    @property
    def remember_me_checkbox(self):
        return CheckBox(By.ID, 'remember_me', self.driver)


class Facebook(Page):
    """A page object model (POM) for facebook login page """
    URL = 'https://www.facebook.com/'

    def __init__(self):
        super().__init__(Facebook.URL)

    # Attributes
    def email_field(self):
        return self.driver.find_element(By.ID, 'email')

    def password_field(self):
        return self.driver.find_element(By.ID, 'pass')

    # Actions
    def login(self, username, password):
        # email enter
        self.email_field().send_keys(username)

        # password enter
        self.password_field().send_keys(password)

        # submit
        self.email_field().submit()
        time.sleep(3)

    @property
    def select_birth_day_checkbox(self):
        return Dropdown(By.XPATH, '//select[@id="day"]', self.driver)


def test_sign_in_create_acc():
    symantec_page = Symantec()
    symantec_page.sign_in_link.click()
    time.sleep(3)
    symantec_page.create_account_button.click()
    time.sleep(1)

    symantec_page.scroll_down(300)
    time.sleep(1)
    symantec_page.scroll_up(300)
    time.sleep(1)
    symantec_page.close_browser()


def test_remember_me():
    symantec_page = Symantec()
    symantec_page.sign_in_link.click()
    time.sleep(3)
    symantec_page.remember_me_checkbox.un_select_element()
    time.sleep(1)
    symantec_page.remember_me_checkbox.select_element()
    time.sleep(1)
    symantec_page.close_browser()


def test_fb_day():
    fb_page = Facebook()
    fb_page.select_birth_day_checkbox.select_value(11)


if __name__ == '__main__':
    test_sign_in_create_acc()
    test_remember_me()
    test_fb_day()
