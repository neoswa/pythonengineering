"""
    What is XPath?
    Absolute and Relative XPath
    The text() function
    The contains(large_text, sub_text) and starts-with(text, str_to_check) functions
       text in large_text
       str.startwith('text')
    Using OR & AND operators
"""

# sdb - 7 / I floor / 101-A / seat-002

# Chennai
#     Medavakkam
#         Valliammai street
#             201 - Monalakshmi homes
#                 F2
#                   Suresh

# /html/body/ul/li[2] - Tea
# /html/body/ol/li[2] - Milk

# Best practice
#       Find elements using it's id - driver.find_element_by_id('id')
#       Find elements using it's xpath - driver.find_element_by_xpath('xpath')
#       Example: /html/body/ul[@id='snacks']/li[text()='Chocolate']

# Syntax
"""
Absolute XPATH:
    /html/body/tag[@att="value"]/tag/tag[@attribute_name="value"]
    /grand_parent_tag/parent_tag/child_tag[conditions]
"""

"""
Relative XPATH:
    //ul[@id="modes"]//li[text()='Train']   # Finds Train inside "modes" unordered list
    //ul[@id="modes"]//li[.='Train']
"""

"""
    https://login.symantec.com/sso/idp/SAML2
    
    and condition:
        //input[@name="registerEmailAddress" and @placeholder="Email address *"]
        
    or condition:
        //input[@id="registerEmailAddress" or @id="confirmEmailAddress" or @placeholder="Create a secure password *"]
        
"""


"""
    http://www.symantec.com
    
    
    //input[contains(attribute of large_text, sub_text)] 
        large_text = text()
    //input[starts-with(large_text, sub_text)]
    
    //h1[contains(text(), "The Symantec Enterprise Security ")]
    //a[contains(@href, "broadcom.com/symantec") and contains(text(), "Learn More")]
    
    //*[starts-with(., "Welcome")]
"""



"""
>>> from selenium import webdriver
>>> url = "https://login.symantec.com/sso/idp/SAML2"
>>>
>>> driver = webdriver.Chrome()
[17304:7652:1216/081450.276:ERROR:configuration_policy_handler_list.cc(90)] Unknown policy: EnableCommonNameFallbackForLocalAnchors
[17304:7652:1216/081450.461:ERROR:configuration_policy_handler_list.cc(90)] Unknown policy: EnableCommonNameFallbackForLocalAnchors

DevTools listening on ws://127.0.0.1:57368/devtools/browser/0eb75519-067b-4f83-924a-5827ffa1efd3
>>>
>>> driver.get(url)
>>> driver.get('https://www.symantec.com/')
>>>
>>> xpath = '//input[@id="registerEmailAddress" or @id="confirmEmailAddress" or @placeholder="Create a secure password *"]'
>>>
>>> driver.find_element_by_xpath
<bound method WebDriver.find_element_by_xpath of <selenium.webdriver.chrome.webdriver.WebDriver (session="ce19f572b1b65a383e0aac9eee844710")>>
>>> driver.find_elements_by_xpath
<bound method WebDriver.find_elements_by_xpath of <selenium.webdriver.chrome.webdriver.WebDriver (session="ce19f572b1b65a383e0aac9eee844710")>>
>>>
>>> driver.find_elements_by_xpath(xpath)
[<selenium.webdriver.remote.webelement.WebElement (session="ce19f572b1b65a383e0aac9eee844710", element="eb9c6eaf-0895-4f2f-8a1b-7c34bfd2c485")>, <selenium.webdriver.remote.webelement.WebElement (session="ce19f572b1b65a383e0aac9eee844710", element="f710243c-210d-457b-9535-b21dc1822df2")>, <selenium.webdriver.remote.webelement.WebElement (session="ce19f572b1b65a383e0aac9eee844710", element="ae974e46-3fb6-4968-bfe2-7d00df6c91d5")>]
>>> fields = driver.find_elements_by_xpath(xpath)
>>>
>>> type(fields)
<class 'list'>
>>> len(fields)
3
>>> for field in fields:
...     field.send_keys('cswadhikar@gmail.com')
...
>>>
>>> fields = driver.find_elements_by_xpath(xpath)
>>> fields[-1]
<selenium.webdriver.remote.webelement.WebElement (session="ce19f572b1b65a383e0aac9eee844710", element="ae974e46-3fb6-4968-bfe2-7d00df6c91d5")>
>>> fields[-1].send_keys('Good@123')
>>> fields[-1].clear()
>>> fields[-1].send_keys('Good@123')
>>>
"""