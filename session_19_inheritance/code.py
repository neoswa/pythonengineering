"""
    Inheritance

    Types of inheritance:
        - Single
        - Multiple
        - Multi-level
        - Hierarchical
"""


# Single inheritance & Muti-level inheritance
class Father:
    def __init__(self, name):
        """Constructor"""
        self.name = name
        self.family_name = 'Ambani'

    def get_fullname(self):
        return f'{self.name} {self.family_name}'


class Son(Father):  # Son inherits from Father
    def __init__(self, name):
        super().__init__(name)
        self.address = 'Mumbai'

    def get_address(self):
        return f'{self.name} lives in {self.address}'


class GrandSon(Son):
    def __init__(self, name):
        super().__init__(name)
        self.address = 'Delhi'

    def set_address(self, new_address):
        self.address = new_address
        print(f"{self.name}'s address been modified successfully!")


# Multiple inheritance
class Employee:
    emp_db = {}  # class variable

    def __init__(self, emp_id, name, company):
        """Creates object and initialized instance variables"""
        self.emp_id = int(emp_id)
        self.name = name
        self.company = company
        Employee.emp_db[emp_id] = name  # build emp database

    def get_email(self):
        """
            Processes emp details to fetch the mail address

            @return     str     Employee mail id
        """
        email = f'{self.name}_{self.emp_id}@{self.company}.com'.lower()
        return email

    @classmethod
    def get_name_from_id(cls, emp_id):
        print(f'Emp with id "{emp_id}" is {cls.emp_db.get(emp_id)}')
        return cls.emp_db.get(emp_id)


class Programmer:
    def __init__(self, emp_id, languages, team):
        self.name = Employee.get_name_from_id(emp_id)
        self.emp_id = emp_id
        self.team = team
        self.languages = languages
        self.company = 'hcl'

    # Diamond problem
    def get_email(self):
        """
            Processes developer details to fetch the mail address

            swad_<team>@hcl_dev.com

            @return     str     Employee mail id
        """
        email = f'{self.name}_{self.team}@{self.company}_dev.com'.lower()
        return email


class Developer(Programmer, Employee):
    def __init__(self, emp_id, name, company, languages, team):
        # super().__init__(emp_id, name, company, languages, experience)    # initializes first class only
        Employee.__init__(self, emp_id, name, company)
        Programmer.__init__(self, emp_id, languages, team)


if __name__ == '__main__':
    Employee(10, 'arun', 'tcs')
    Employee(20, 'rajesh', 'cts')
    Employee(30, 'murugan', 'infy')
    print(Employee.emp_db)
    print(f'{Employee.get_name_from_id(100)}')

    dev = Developer(1, 'sri', 'hcl', ['python', 'java'], 'ISE')
    print(f'{dev.name} email {dev.get_email()}')

    dev_2 = Developer(2, 'praba', 'cts', ['ruby', 'java'], 'FD')
    print(f'{dev_2.name} email {dev_2.get_email()}')

    father = Father('Dhirubai')
    print(f'{father.get_fullname()}')

    son = Son('Mukesh')
    # print(f'{dir(son)}')
    print(f'{son.get_fullname()}')
    print(f'{son.get_address()}')

    g_son = GrandSon('Akash')
    print(f'{g_son.get_fullname()}')
    print(f'{g_son.get_address()}')

    # Address change
    g_son.set_address('Delhi')
    print(f'{g_son.get_address()}')
