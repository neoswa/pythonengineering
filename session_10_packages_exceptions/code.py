"""
    Exception Handling:
        - try -- except -- finally
        - custom exceptions

    Packages:
        - Lets to organize code
        - If a directory contains a file "__init__.py" then it becomes a jenkins
        - Builtin jenkins example: unittest, json etc.
"""

# print(a)              # NameError
#
# l = [0, 1]
# print(l[100])         # ListIndexOutOfRangeError
#
# d = {}
# d[1]                  # KeyError


# Exception
# list
# tuple
# str
#
# result = reversed(['a', 'b', 'c'])        # reversed() is automatically imported from builtins module
# print(list(result))
#
# # issubclass(child_class, parent_class)
# result = issubclass(KeyError, Exception)
# print(result)
# result = issubclass(TabError, Exception)
# print(result)
#
# # Prints all inherited classes
# for class_ in Exception.__subclasses__():
#     print(class_)

"""
    try:
        code that may raise exception
    except:
        block that runs when exception arises
    finally:
        block that always run regardless of whether
        exception is raised or not 
"""

# filename = 'questions.py'
# file_obj = None
#
# try:
#     # 1/0
#     # # Code subject to error
#     # d = {}
#     # d[1]
#     file_obj = open(filename)                    # acquire file lock
#     print('File opened')
#     # print(file_obj.read())
#     file_contents = file_obj.read()
#     print(int(file_contents))
# except FileNotFoundError as e:
#     # Handle exception
#     print(f'File does not exist!')
# except KeyError as e:
#     print(f'Key error')
# except Exception as e:
#     print(e.__class__.__name__)
#     print(f'Unexpected exception: {e}')
#     raise
# finally:
#     file_obj.close()                             # release the file
#     print('File is closed')

USER = 'swadhi'
PWD = 'guessme'


def login_gmail(user, pwd):
    if user == USER and pwd == PWD:
        print('Login successful!')
    else:
        print('Login failed')
        raise ValueError


def logout():
    print('Logout successful!')


# How to create a custom exception?
class NetworkError(Exception):
    pass


def send_mail(to, content):
    # print(f'sent the email to {to}')
    # raise Exception('Network Error')
    raise NetworkError('System is not connected')


try:
    login_gmail('swadhi', 'guessme')  # acquire resource
    send_mail('chiru', content='Hello!')
except ValueError:
    # handle login error
    print(f'Either username or password is incorrect. Please retry')
except Exception as e:
    print(f'EMail sending incomplete due to: {e}')
finally:
    logout()  # release resource


# Packages:
#     - Lets to organize code
#     - If a directory contains a file "__init__.py" then it becomes a jenkins
#     - Builtin jenkins example: unittest, json etc.
#     - Opens way for using third party libraries (packages). Example: socket, selenium ...
# Modules:
#     - Module is a python script

# jenkins/module_1
# jenkins/module_2
# ...
# jenkins/module_n


"""
Ways to access a module:
    import module
    from jenkins import module
    from jenkins.module import functions
    import module
"""

# print(dir())
# print(__file__)
# print(__doc__)


if __name__ == '__main__':
    print(f'__name__: {__name__}')
