# Write a function that accepts a number and divides it by 0.
# Observe the exception that raises and handle the specific exception by logging
# with a proper debug message

def divider(number):
    try:
        reminder = number / 0  # It has thrown a ZeroDivisionError
    except ZeroDivisionError as e:
        print('Caught exception:"', e, '" while dividing by 0')

    return reminder


divider(8)  # Caught exception:" division by zero " while dividing by 0

# 1. Write any function that accesses any variable that is not declared. For example, name = 'user'
# Print the undefined variable. The script must handle the exception and print a valid log message


# 2. Write any function that accepts a dictionary. Inside the function try to
# print the key that does not exists. For example: d[inexistant_key]. Observe
# the type of exception thrown. Now add a try-except for the specific type of Exception and log


# 3. Write any function that accepts a tuple. Inside the function try to
# modify the first element to a different value. (t[0]=new value). Observe
# the type of exception thrown. Now add a try-except for the specific type of Exception and log


# 4. Write any function that accepts a set. Inside the function try to
# print the value of index 0 in the set. Observe
# the type of exception thrown. Now add a try-except for the specific type of Exception and log


# 5. Write any function that accepts two arguments (k, v). Inside the function try to
# create a dictionary with one item using k as key and v as value and return the dictionary.
# Now access the function by passing a list as first argument and any datatype as second argument.
# Observe the type of exception thrown. Now add a try-except for the specific type of
# Exception and return None if exception arises


# 6. Create your own exception for the below scenarios.
#   1. IPUnavailableException
#   2. InsufficientBrowsersException
#   3. NegativeIntegerNotAllowedException
#   4. TooManyArgumentsException
#   5. InvalidDomainNameException

# 7. Write a function that accepts *args. Inside the function
# check the number of arguments in *args:
# if it is greater 3, then raise TooManyArgumentsException
# else, add all the arguments and return the value.
# For Example, 1 + 2 + 3 = 6 or 'a' + 'b' + 'c' = 'abc'


# 8. Write a function that accepts **kwargs which is a mapping which is of form:
# ip1=10.197.2.1
# ip2=10.197.2.2
# Inside the function, check the ip addresses that ends with a
# number that is divisible by 2 and return them as a string.
# If no IPs are found with even digits at the end, then raise IPUnavailableException
# Call this function and test its functionality.

# 9. Write a function "get_browsers" that accepts no arguments.
# Inside the function, just build a list of browser names.
# For example: ie, firefox and chrome. These are the  browsers that the
# program will only support.
# Call this function and check the number of browsers returned.
# If the count is less than 5, then raise InsufficientBrowsersException


# 10. Write a function that accepts only two websites as arguments. For example: www.ui.com, www.ui.in
# Given a list of valid_domains = ('in', 'com', 'org', 'edu'). If the supplied arguments does not contain the
# domain names in the valid_domains, then raise InvalidDomainNameException.
# Call the above method with various invalid domains such as 'www.ui.won', 'www.fb.come'
# Handle the exception use try-except and the script must always print "IP Validation Successful" whether
# if an exception is raised or not
# Hint: use str.split() method to split website name
