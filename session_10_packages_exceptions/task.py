"""
Write a function transfer_amount(amount):
    if amount is gt BALANCE, print amount transferred
    else, raise InsufficientFundsException
"""


class InsufficientFundsException(Exception):
    pass


BALANCE = 1000


def transfer(amount):
    global BALANCE                      # acquire write access

    if amount >= BALANCE:
        raise InsufficientFundsException(f'you do not have sufficient funds')

    BALANCE = BALANCE - amount
    print(f'amount transferred. Remaining balance: {BALANCE}')


transfer_amount = 2000
try:
    transfer(transfer_amount)
except InsufficientFundsException as e:
    print(f'The amount you requested "{transfer_amount}" is not available. Avail bal: {BALANCE}')

transfer(540)
transfer(60)
transfer(100)
